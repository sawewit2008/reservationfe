import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { HomeModule } from './app/modules/homemodule/home.module';
import { ItemModule } from './app/modules/itemselectionmodule/item.module';
import { GuestdetailsModule } from './app/modules/guestdetailsmodule/guestdetails.module';
import { PaymentModule } from './app/modules/paymentmodule/payment.module';


import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
