import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { RouterModule, Routes } from '@angular/router'; 

import { AppComponent } from './app.component';
import { HomeComponent } from './components/homecomponent/home.component';  
import { ItemComponent } from './components/itemselectioncomponent/item.component';  
import { GuestdetailsComponent } from './components/guestdetailscomponent/guestdetails.component'; 
import { PaymentComponent } from './components/paymentcomponent/payment.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'item/item-selection/:id', component: ItemComponent },  
  { path: 'item-selected/:id/guest-detail', component: GuestdetailsComponent },
  { path: 'item-selected/:id/payment', component: PaymentComponent },
  { path: '**', component: GuestdetailsComponent } //dapat sa error page mo redirect

];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ItemComponent,  
    GuestdetailsComponent,  
    PaymentComponent  
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot(),
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
