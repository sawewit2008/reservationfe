import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import { HttpClientModule } from '@angular/common/http';
//import { CustomHttpServices } from './httpservices';

import { ItemComponent } from './../../components/itemselectioncomponent/item.component';

@NgModule({
  declarations: [
    ItemComponent
  ],
  imports: [
    BrowserModule//,
    //HttpClientModule
  ],
  providers: [],//[CustomHttpServices],
  schemas: [],
  bootstrap: [ItemComponent]
})
export class ItemModule { }
