import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


//import { AppComponent } from './app.component';
import { PaymentComponent } from './../../components/paymentcomponent/payment.component';

@NgModule({
  declarations: [
    PaymentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [PaymentComponent]
})
export class PaymentModule { }
