import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { AppComponent } from './app.component';
import { GuestdetailsComponent } from './../../components/guestdetailscomponent/guestdetails.component';

@NgModule({
  declarations: [
    GuestdetailsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [GuestdetailsComponent]
})
export class GuestdetailsModule { }
