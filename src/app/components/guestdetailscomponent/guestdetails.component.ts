import { Component } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-guest-details',
  //templateUrl: './app.component.html',
  //styleUrls: ['./app.component.css']
  templateUrl: './../../components/guestdetailscomponent/guestdetails.component.html'
})
export class GuestdetailsComponent {

  constructor(private router: Router) {
  }

  //Go to next
  gotoNext(){
    var id = 26

    this.router.navigate(['item-selected/' + id + '/payment/']); 
  }

   //Go to next
   gotoPrev(){
    var id = 43

    this.router.navigate(['/item/item-selection/', id]); 
  }
}



