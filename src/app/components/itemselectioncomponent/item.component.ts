import { Input, ViewChild, ElementRef, AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../services/api.service';
import { MyNewInterface } from '../../interfaces/my-new-interface';


declare var $: any;
declare var baguetteBox: any;


@Component({
  selector: 'app-item-selection',
  //templateUrl: './app.component.html',
  styleUrls: ['../../../assets/css/cards-gallery.css'],
  templateUrl: './../../components/itemselectioncomponent/item.component.html',
  providers: [ApiService]
})


export class ItemComponent implements AfterViewInit, OnInit {
  _postsArray : MyNewInterface[];
  ngOnInit(){
    alert(234234);
    this.getPosts();
  }

  getPosts(): void{
    this.apiService.getPosts()
    .subscribe(
      resultArray => this._postsArray = resultArray,
      error => console.log("Error :: " + error)
    )
  }

  item = 'Room';

  img1 = '../../../assets/img/image1.jpg';
  img2 = '../../../assets/img/image2.jpg';
  img3 = '../../../assets/img/image3.jpg';
  img4 = '../../../assets/img/image4.jpg';
  img5 = '../../../assets/img/image5.jpg';
  img6 = '../../../assets/img/image6.jpg';
  img7 = '../../../assets/img/image7.jpg';
  img8 = '../../../assets/img/image8.jpg';
  img9 = '../../../assets/img/image9.jpg';

  ngAfterViewInit() {
    baguetteBox.run('.cards-gallery', { animation: 'slideIn'});
  }


  constructor(private router: Router, private apiService: ApiService) { }
  gotoClientDetails(){
    var id = 26

    this.router.navigate(['item-selected/' + id + '/guest-detail/']);
  }

 
}