import { Component } from '@angular/core';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router'; 



@Component({
  //selector: 'ngbd-datepicker-range',
  selector: 'app-select-dates',
  templateUrl: './../../components/homecomponent/home.component.html',
  styles: [`
    .custom-day {
      text-align: center;
      padding: 0.185rem 0.25rem;
      display: inline-block;
      height: 2rem;
      width: 2rem;
    }
    .custom-day.focused {
      background-color: #e6e6e6;
    }
    .custom-day.range, .custom-day:hover {
      background-color: rgb(2, 117, 216);
      color: white;
    }
    .custom-day.faded {
      background-color: rgba(2, 117, 216, 0.5);
    }
    .form-control { width: 300px; display: inline; }
  `],
  providers:[]
})



export class HomeComponent {
  title = 'HOTELS, RESORTS & MORE';
  mysawewitMessage = '';
  hoveredDate: NgbDateStruct;

  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;


  //We can have multiple constructor, di pwde tagsa tagsaon pag create ang constructor
  constructor(calendar: NgbCalendar, private router: Router) {
  }



  img1 = '../../../assets/img/calendar.svg';


 //Go to find booking
  findBooking(){
    var id = 43

    this.router.navigate(['/item/item-selection/', id]); 
    //this.mysawewitMessage = "hahahah";
    //alert("From: " + this.fromDate);
    //alert("From: " + this.toDate);
  }


}