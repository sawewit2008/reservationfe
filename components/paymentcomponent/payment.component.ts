import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  //templateUrl: './app.component.html',
  //styleUrls: ['./app.component.css']
  templateUrl: './../../components/paymentcomponent/payment.component.html'
})
export class PaymentComponent { 
 
  constructor(private router: Router) { }

  //Go to next
  gotoNext(){
    var id = 43

    this.router.navigate(['/payment/']); 
  }

  //Go to next
  gotoPrev(){
    var id = 26

    this.router.navigate(['/item-selected/' + id + '/guest-detail/']); 
  }
}